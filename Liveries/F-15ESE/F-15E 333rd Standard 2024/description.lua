livery = {
--   "F-15E 333rd Standard 2024" skin stworzony na potrzeby gildiowej paczki 1.0.8  zbieranej w sierpniu 2024

-- NOSE
	{"F-15E_01",	0,	                "F-15e_E01_A_CLEAN",   true};
	{"F-15E_01",	1,	                "F-15e_E01_NRM",	   true};
	{"F-15E_01",	ROUGHNESS_METALLIC,	"F-15e_E01_RoughMet",  true};
	{"F-15E_01",	DECAL,	    		"kalka_10",	          false};  ----  ATRYBUTY 333rd  ----
    
-- TOP
	{"F-15E_02",	0,	                "F-15e_E02",		   true};
	{"F-15E_02",	1,	                "F-15e_E02_NRM",	   true};
	{"F-15E_02",	ROUGHNESS_METALLIC,	"F-15e_E02_RoughMet",  true};
    
-- AIRBRAKE
	{"F-15E_02SB",	0,	                "F-15e_E02",		   true};
	{"F-15E_02SB",	1,	                "F-15e_E02_NRM",	   true};
	{"F-15E_02SB",	ROUGHNESS_METALLIC,	"F-15e_E02_RoughMet",  true};
    
-- BOTTOM
	{"F-15E_03",	0,	                "F-15e_E03",		   true};
	{"F-15E_03",	1,	                "F-15e_E03_NRM",	   true};
	{"F-15E_03",	ROUGHNESS_METALLIC,	"F-15e_E03_RoughMet",  true};
    
-- CFT
	{"F-15E_07",	0,	                "F-15e_E07",		   true};
	{"F-15E_07",	1,	                "F-15e_E07_NRM",	   true};
	{"F-15E_07",	ROUGHNESS_METALLIC,	"F-15e_E07_RoughMet",  true};
	{"F-15E_07",	DECAL,	    		"kalka_10",	          false};  ----  ATRYBUTY 333rd ----
    
-- TAIL
	{"F-15E_06",	0,	                "F-15e_E06",		   true};
	{"F-15E_06",	1,	                "F-15e_E06_NRM",	   true};
	{"F-15E_06",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet",  true};
	{"F-15E_06",	DECAL,	    		"kalka_10",	          false};  ----  ATRYBUTY 333rd ----

------------------------------WINGS-------------------------------------------------------------	
-- WING 1 = Left Top and Right Bottom
	{"F-15E_04",	0,	                "F-15e_E04",		         true};
	{"F-15E_04",	1,	                "F-15e_E04_NRM",	         true};
	{"F-15E_04",	ROUGHNESS_METALLIC,	"F-15e_E04_RoughMet",        true};
--AILERON 1	
 	{"F-15E_04AT",	0,	                "F-15e_E04",			     true};
 	{"F-15E_04AT",	1,	                "F-15e_E04_NRM",	         true};
 	{"F-15E_04AT",	ROUGHNESS_METALLIC,	"F-15e_E04_RoughMet",        true};
 
 	{"F-15E_04AB",	0,	                "F-15e_E04",			     true};
 	{"F-15E_04AB",	1,	                "F-15e_E04_NRM",	         true};
 	{"F-15E_04AB",	ROUGHNESS_METALLIC,	"F-15e_E04_RoughMet",        true};
--FLAP 1
 	{"F-15E_04FT",	0,	                "F-15e_E04",			     true};
 	{"F-15E_04FT",	1,	                "F-15e_E04_NRM",	         true};
 	{"F-15E_04FT",	ROUGHNESS_METALLIC,	"F-15e_E04_RoughMet",        true};
 
 	{"F-15E_04FB",	0,	                "F-15e_E04",			     true};
 	{"F-15E_04FB",	1,	                "F-15e_E04_NRM",	         true};
 	{"F-15E_04FB",	ROUGHNESS_METALLIC,	"F-15e_E04_RoughMet",        true};
 	
-- WING 2  Right Top and Left Bottom
 	{"F-15E_05",	0,	                "F-15e_E05",			     true};
 	{"F-15E_05",	1,	                "F-15e_E05_NRM",	         true};
 	{"F-15E_05",	ROUGHNESS_METALLIC,	"F-15e_E05_RoughMet",        true};
--ALIERON 2			
 	{"F-15E_05AT",	0,	                "F-15e_E05",			     true};
 	{"F-15E_05AT",	1,	                "F-15e_E05_NRM",	         true};
 	{"F-15E_05AT",	ROUGHNESS_METALLIC,	"F-15e_E05_RoughMet",        true};
 
 	{"F-15E_05AB",	0,	                "F-15e_E05",			     true};
 	{"F-15E_05AB",	1,	                "F-15e_E05_NRM",	         true};
 	{"F-15E_05AB",	ROUGHNESS_METALLIC,	"F-15e_E05_RoughMet",        true};
--FLAP 2
 	{"F-15E_05FT",	0,	                "F-15e_E05",			     true};
 	{"F-15E_05FT",	1,	                "F-15e_E05_NRM",	         true};
 	{"F-15E_05FT",	ROUGHNESS_METALLIC,	"F-15e_E05_RoughMet",        true};
 
 	{"F-15E_05FB",	0,	                "F-15e_E05",			     true};
 	{"F-15E_05FB",	1,	                "F-15e_E05_NRM",	         true};
 	{"F-15E_05FB",	ROUGHNESS_METALLIC,	"F-15e_E05_RoughMet",        true};
 -----------------------------------------------------------------------------------------------------------   
		
--WEAPONS

	--{"BDU_50",       0 ,    "GBU_50LGB_Diff_White",   false};
	
	--{"CATM-9-main",  0 ,	  "catm-9_flare",           false};
	
	--{"GBU_10",       0 ,    "GBU_10_white",           false};
	
	--{"GBU_12",       0 ,    "GBU_12_White",           false};

-- BORT_NUMBERS ----------------------------------------------------------------------------------------

--15E_REFUEL-DECAL
    {"F-15E_REFUEL-DECAL",	0,	  "F-15E_REFUEL_AF-87-0",  true};
--15E_REFUEL-NUMBER
    {"F-15E_REFUEL-NUMBER",	0,	  "F-15E_WHT_Numbers",     true}; 
  
--TAIL-DECAL	
    {"F-15E_TAIL-DECAL",	0,	                "AIRFORCE",					   false}; ---- CZCIONKA 333 ---- AIR BASE NR
    
--TAIL-NUMBER	
    {"F-15E_TAIL-NUMBER",	0,	                "tail_Numbers_333",			   false}; ---- CZCIONKA 333 ---- BORT NUMERY
    
--TAIL-DECAL-L	
    {"F-15E_TAIL-DECAL-L",	0,	                "F-15e_E06",					true};
	{"F-15E_TAIL-DECAL-L",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-DECAL-L",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet", 			true};
    {"F-15E_TAIL-DECAL-L",	DECAL,	            "AIRFORCE",				       false}; ---- CZCIONKA 333 ---- AIR BASE NR
	
--TAIL-DECAL-R
    {"F-15E_TAIL-DECAL-R",	0,	                "F-15e_E06",					true};
	{"F-15E_TAIL-DECAL-R",	1,	                "F-15e_E06_NRM",				true};
	{"F-15E_TAIL-DECAL-R",	ROUGHNESS_METALLIC,	"F-15e_E06_Roughmet", 			true};
    {"F-15E_TAIL-DECAL-R",	DECAL,	            "AIRFORCE",				       false}; ---- CZCIONKA 333 ---- AIR BASE NR

--TAIL-NUMBER-L
 	{"F-15E_TAIL-NUMBER-L",	0,	                "F-15e_E06",					true};
 	{"F-15E_TAIL-NUMBER-L",	1,	                "F-15e_E06_NRM",				true};
 	{"F-15E_TAIL-NUMBER-L",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet", 			true};
    {"F-15E_TAIL-NUMBER-L",	DECAL,	            "tail_Numbers_333",			   false}; ---- CZCIONKA 333 ---- BORT NUMERY
	
--TAIL-NUMBER-R                             
 	{"F-15E_TAIL-NUMBER-R",	0,	                "F-15e_E06",				    true};
 	{"F-15E_TAIL-NUMBER-R",	1,	                "F-15e_E06_NRM",				true};
 	{"F-15E_TAIL-NUMBER-R",	ROUGHNESS_METALLIC,	"F-15e_E06_RoughMet", 			true};
    {"F-15E_TAIL-NUMBER-R",	DECAL,	            "tail_Numbers_333",			   false}; ---- CZCIONKA 333 ---- BORT NUMERY
	


--------------------------------------------------------------------------------------------------------    
--PILOT
	{"F-15EC_05",	0,	                "F-15E_EC05A1",			        true};
	{"F-15EC_05",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06",	0,	                "F-15E_EC06A1_PB",			   false};   ---- PILOT 333rd ----
	{"F-15EC_06",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 		true};

--WSO
	{"F-15EC_05R",	0,	                "F-15E_EC05A1",			        true};
	{"F-15EC_05R",	1,	                "F-15E_EC05_NRM",				true};
	{"F-15EC_05R",	ROUGHNESS_METALLIC,	"F-15E_EC05_RoughMet", 			true};

	{"F-15EC_06R",	0,	                "F-15E_EC06A1_PB",			   false};   ---- WSO 333rd ----
	{"F-15EC_06R",	1,	                "F-15E_EC06_NRM",				true};
	{"F-15EC_06R",	ROUGHNESS_METALLIC,	"F-15E_EC06_RoughMet", 			true};

	{"F-15EC_07R",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07R",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07R",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 		true};


-----------------------------VR/OTHER PILOTS-----------------------------------------

--PILOT
	{"F-15E_VR-Pilot1",	0,	                "F-15E_TP1B1",					true};
	{"F-15E_VR-Pilot1",	1,	                "F-15E_TP1_NRM",				true};
	{"F-15E_VR-Pilot1",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 			true};

	{"F-15E_VR-Pilot2",	0,	                "F-15E_TP2B1",					true};
	{"F-15E_VR-Pilot2",	1,	                "F-15E_TP2_NRM",				true};
	{"F-15E_VR-Pilot2",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 			true};

	{"F-15E_VR-Pilot3",	0,	                "F-15E_TP3B",					true};
	{"F-15E_VR-Pilot3",	1,	                "F-15E_TP3_NRM",				true};
	{"F-15E_VR-Pilot3",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 			true};

--WSO
	{"F-15E_VR-Pilot1R",	0,	                "F-15E_TP1B1",				true};
	{"F-15E_VR-Pilot1R",	1,	                "F-15E_TP1_NRM",			true};
	{"F-15E_VR-Pilot1R",	ROUGHNESS_METALLIC,	"F-15E_TP1_RoughMet", 		true};

	{"F-15E_VR-Pilot2R",	0,	                "F-15E_TP2B1",				true};
	{"F-15E_VR-Pilot2R",	1,	                "F-15E_TP2_NRM",			true};
	{"F-15E_VR-Pilot2R",	ROUGHNESS_METALLIC,	"F-15E_TP2_RoughMet", 		true};

	{"F-15E_VR-Pilot3R",	0,	                "F-15E_TP3B",				true};
	{"F-15E_VR-Pilot3R",	1,	                "F-15E_TP3_NRM",			true};
	{"F-15E_VR-Pilot3R",	ROUGHNESS_METALLIC,	"F-15E_TP3_RoughMet", 		true};

--Visor
	{"F-15EC_07_Visor",	0,	                "F-15E_EC07A1",					true};
	{"F-15EC_07_Visor",	1,	                "F-15E_EC07A_NRM",				true};
	{"F-15EC_07_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 		true};

	{"F-15EC_07R_Visor",	0,	                "F-15E_EC07A1",				true};
	{"F-15EC_07R_Visor",	1,	                "F-15E_EC07A_NRM",			true};
	{"F-15EC_07R_Visor",	ROUGHNESS_METALLIC,	"F-15E_EC07A_RoughMet", 	true};

}
name = "F-15E 333rd Standard 2024"
--countries = {"USA"}

                --   "333rd Punkbusters F-15E" skin adapted by 333rd_Vincent July 30 2024  DCS v 2.9...
custom_args = 
{
[32]   = 0.1, -- 1     trzecia cyfra refuel and tail number  cyfry zmieniane dynamicznie z menue payload
[31]   = 0.0, -- 10    druga cyfra -------------------------
[442]  = 0.7, -- 100   pierwsza cyfra -----------------------
[1000] = 0.1, -- Tail board number (0.0 - No Numbers, 0.1 - 3 Numbers, 0.2 - 4 Numbers starts with "1", 0.3 - Full 4 Numbers)
[1001] = 0.1, -- vis refuel board number (0.0 - No Numbers, 0.1 Numbers, 0.2 - Numbers Early)
[1002] = 0.475, -- intake board number (0.4 - IDF Nose/Fuselage Numbers and Model Change)
[1003] = 0.02, -- vis Nose Gear board number (0.0 - 4 Numbers 0.0 to 0.05 adjusts the placement, 0.1 - 5 Numbers) 
}

