livery = {

	{"f18c1", 0 ,"f18c_1_dif",false}; -- GŁOWNA CZĘŚĆ SKŁADOWA --
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"f18c_2_dif",false}; -- GŁOWNA CZĘŚĆ SKŁADOWA --
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	
	{"FPU_8A", 0 ,"FPU_8A",false}; --  ZBIORNIK PALIWA
	{"FPU_8A", 2 ,"FPU_8A_Diff_RoughMet",true};	

	-- MODEX (NEW FORMAT)
	{"f18c1_number_nose_right", 0 ,"F18C_1_DIF",false};
	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_right", DECAL ,"NumeryR_NOS",false};
	
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIF",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_left", DECAL ,"NumeryL_NOS",false};	

	{"f18c2_kil_right", 0 ,"F18C_2_DIF",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_right", DECAL ,"NumerySTER",false};

	{"f18c2_kil_left", 0 ,"F18C_2_DIF",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_left", DECAL ,"NumerySTER",false};
	
	{"f18c1_number_F", 0 ,"F18C_1_DIF",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_F", DECAL ,"NumeryFLAPY",false};	

	{"f18c2_number_X", 0 ,"F18C_2_DIF",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_number_X", DECAL ,"NumeryFLAPY",false};

	{"pilot_F18", 0 ,"../VFA-203 Line/pilot_F18_tan",false}; -- MUNDUR PILOTA I TWARZ
	{"pilot_F18_patch", 0 ,"../VMFA-117 Wombats HiVis/pilot_F18_patch",false};
	{"pilot_F18_patch", 1 ,"../VMFA-117 Wombats HiVis/pilot_F18_patch_nm",false};
	{"pilot_F5_FP", 0, "../VMFA-117 Wombats HiVis/pilot_F18_des", false};
	{"pilot_F18_helmet", 0 ,"../VMFA-117 Wombats HiVis/pilot_F18_helmet_w",false};
	{"pilot_F18_helmet", 1 ,"../VMFA-117 Wombats HiVis/pilot_F18_helmet_nm",false};
	{"pilot_F18_helmet",13 ,"../VMFA-117 Wombats HiVis/pilot_F18_helmet_roughmet",false};	
}

name = "VMFA-117 Wombats" -- NAZWA WYŚWIETLANA W DCS

	custom_args = {
[27]   = 0.0, -- Upper Tail Modex
[1000] = 0.0, -- Flap Modex
[1001] = 0.0, -- Upper Nose Modex
[1002] = 1.0, -- Middle Tail and Lower Nose Modex
[1003] = 1.0, -- Rear Fuselage and Gear Doors Modex
[1004] = 1.0, -- Forward Fuselage Modex
[1005] = 1.0, -- Lower Tail Modex
}
	
--- VMFA-117 Poko changes 07.05.2023