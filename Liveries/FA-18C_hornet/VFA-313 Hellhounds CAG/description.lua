livery = {
	--Main Maps
	{"f18c1", 0 ,"F18C_1_DIFF_VFA313_Hellhounds",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	{"f18c2", 0 ,"F18C_2_DIFF_VFA313_Hellhounds",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	
	--Pilot Maps
	{"pilot_F18_patch", 0 ,"../VFA-313 Hellhounds/Pilot_F18_Patch_Hellhounds",false};
	{"pilot_F18_patch", 1 ,"../VFA-313 Hellhounds/Pilot_F18_Patch_NORM_Hellhounds",false};
	{"pilot_F18_patch", 13 ,"../VFA-313 Hellhounds/Pilot_F18_Patch_Hellhounds_RoughMet",false};
		
	--Fuel Tanks
	{"FPU_8A", 0 ,"FPU_8A_VFA313_Hellhounds",false};
	{"FPU_8A", ROUGHNESS_METALLIC ,"FPU_8A_Diff_RoughMet",true};	
	
	-- MODEX (NEW FORMAT)
	{"f18c1_number_nose_right", 0 ,"F18C_1_DIFF_VFA313_Hellhounds",false};
	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_right", DECAL ,"NumeryR_NOS",false};
	
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIFF_VFA313_Hellhounds",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_nose_left", DECAL ,"NumeryL_NOS",false};	

	{"f18c2_kil_right", 0 ,"F18C_2_DIFF_VFA313_Hellhounds",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_right", DECAL ,"empty",true};

	{"f18c2_kil_left", 0 ,"F18C_2_DIFF_VFA313_Hellhounds",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_kil_left", DECAL ,"empty",true};
	
	{"f18c1_number_F", 0 ,"F18C_1_DIFF_VFA313_Hellhounds",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"f18c1_number_F", DECAL ,"NumeryFLAPY",false};	

	{"f18c2_number_X", 0 ,"F18C_2_DIFF_VFA313_Hellhounds",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"f18c2_number_X", DECAL ,"NumeryFLAPY",false};		
}
name = "VFA-313 Hellhounds CAG"

	custom_args = {
[442] = 0.3, -- numer x00
[31] = 0.0, -- numer 0x0
[32] = 0.0, -- numer 00x
[27]   = 1.0, -- Upper Tail Modex
[1000] = 0.0, -- Flap Modex
[1001] = 0.0, -- Upper Nose Modex
[1002] = 1.0, -- Middle Tail and Lower Nose Modex
[1003] = 1.0, -- Rear Fuselage and Gear Doors Modex
[1004] = 1.0, -- Forward Fuselage Modex
}
	
--- VMFA-117 Poko changes 07.05.2023