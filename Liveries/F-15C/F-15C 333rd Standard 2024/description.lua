livery = {
    
    -- przód tekstura --
	{"f15nose", 0 ,"f15_nose",false};
    -- przód kalkomanie --
	{"f15nose", 3 ,"f15_decol_standard",false};
    
    -- spód tekstura --
	{"f15bottom", 0 ,"f15_bottom",false};
    -- spód kalkomanie --
	{"f15bottom", 3 ,"empty",true};
    
    -- kadłub góra --
	{"f15centr", 0 ,"f15_centr",false};
    
    -- tekstura lewego skrzydła --
	{"f15wingl", 0 ,"f15_wing_l",false};
    -- kalkomanie na skrzydłach --
	{"f15wingl", 3 ,"empty",true};
    -- tekstura prawego skrzydła --
	{"f15wingr", 0 ,"f15_wing_r",false};
    
    -- ogon i stateczniki tekstura --
	{"f15stab", 0 ,"f15_stab_v",false};
    -- ogon kalkomanie --
	{"f15stab", 3 ,"empty",true};
    
    -- numery burtowe --
	{"f15numbers", 0 ,"f15_numbers",false};
  
   -- naszywki pilota --
	{"pilot_F15_patch", 0 ,"pilot_patch_f15_pl",false};
    -- kask pilota --
	{"pilot_F15_00_helmet", 0 ,"pilot_f15_helmet_Standard",false};
    -- ciało pilota --
	{"pilot_F15_00_body", 0 ,"pilot_f15_00_a",true};
    -- zbiornik podwieszany tekstura --
	{"f15PTB", 0 ,"f15_PTB",false};
    -- Fotel pilota --
    {"f15_aces2", 0 ,"AcesII_eject_seat_black",false};
}

name = "F-15C 333rd Standard 2024"

-- skin "F-15C 333rd Standard 2024" na potrzeby paczki gildiowej 1.0.8 zbieranej w sierpniu 2024
-- powstał od podstaw z template ED przez 333rd_Vincent
-- stanowi własność eskadry 333rd Punkbusters 
-- został odchudzony wagowo 2,8 MB w stosunku do wersji poprzedniej z paczki 1.0.7 bez wpływu na jakość
