livery = {


--Diffuse  -----------------------------------

{"HB_F14_EXT_01", 0 ,"../../F-14A-135-GR/VF31_IRIAF/../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_01",false};
{"HB_F14_EXT_02", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_02",false};
{"HB_F14_EXT_03", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_03",false};
{"HB_F14_EXT_04", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_04",false};
{"HB_F14_EXT_TAIL", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_TAIL",false};
{"HB_F14_WING_LEFT_01", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_WING_LEFT",false};
{"HB_F14_WING_RIGHT", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_WING_RIGHT",false};
{"HB_F14_EXT_INTAKERAMPS", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_INTAKERAMPS",false};
{"HB_F14_TCS", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_TCS",false};
{"HB_F14_EXT_PHOENIXRAILS", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_PHOENIXPYLONS",false};
{"HB_F14_EXT_PYLONS", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_PYLONS_01",false};
{"HB_F14_EXT_LANTIRN_PYLON", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_LANTIRN_PYLON",false};
{"HB_F14_EXT_LAU-7", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_LAU_7",false};
{"HB_F14_EXT_DROPTANKS", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_DROPTANK",false};
{"HB_F14_EXT_PILOT_HELMET", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_PILOT_HELMET",false};
{"HB_F14_EXT_RIO_HELMET", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_RIO_HELMET",false};
{"HB_F14_EXT_PILOT_SUIT", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_PILOT_SUIT",false};
{"HB_F14_EXT_PILOT_SUIT", 1 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_EXT_PILOT_SUIT_Nrm",false};
{"HB_F14_LOD1_3in1", 0 ,"../../F-14A-135-GR/VF31_IRIAF/HB_F14_LOD1_3in1",false};

}
name = "VF-31 IRIAF"




