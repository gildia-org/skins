-- Skin by Raven
-- Pilot and EVU ring by Sundowner.pl

livery = {

-- mi-24p_ext01 
	{"mi-24p_ext01", 0, 						"../SGPSz/mi-24p_ext01_SGPSz", 				false};
	{"mi-24p_ext01", 1, 						"mi_24p_ext01_NRM",									true};	
	{"mi-24p_ext01", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext01_SGPSz_RoughMet", 	false};
	
-- mi-24p_ext02 
	{"mi-24p_ext02", 0, 						"../SGPSz/mi-24p_ext02_PL_V_Eyes", 			false};
	{"mi-24p_ext02", 1, 						"../SGPSz/mi-24p_ext02_V_NRM",				false};	
	{"mi-24p_ext02", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext02_PL_V_RoughMet_gloss",false};
	
-- mi-24p_ext03
	{"mi-24p_ext03", 0, 						"../SGPSz/mi-24p_ext03_PL_V_Eyes", 			false};
	{"mi-24p_ext03", 1, 						"../SGPSz/mi-24p_ext03_V_NRM",				false};	
	{"mi-24p_ext03", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext03_PL_V_RoughMet_gloss",false};	
	
-- mi-24p_ext04 
	{"mi-24p_ext04", 0, 						"../SGPSz/mi-24p_ext04_PL", 				false};
	{"mi-24p_ext04", 1, 						"mi_24p_ext04_NRM",									true};		
	{"mi-24p_ext04", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext04_PL_RoughMet", 		false};	
	
-- mi-24p_ext05 
	{"mi-24p_ext05", 0, 						"../SGPSz/mi-24p_ext05_PL", 				false};
	{"mi-24p_ext05", 1, 						"../SGPSz/mi-24p_ext05_NRM",				false};	
	{"mi-24p_ext05", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext05_PL_RoughMet", 		false};	
	
-- mi-24p_ext06 
	{"mi-24p_ext06", 0, 						"../SGPSz/mi-24p_ext06_PL", 				false};
	{"mi-24p_ext06", 1, 						"mi_24p_ext06_NRM", 								true};	
	{"mi-24p_ext06", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext06_PL_RoughMet", 		false};	
	
	{"mi-24p_ext06_BN",	0,	                	"../SGPSz/mi-24p_ext06_PL", 				false};
	{"mi-24p_ext06_BN",	1,	                	"mi_24p_ext06_NRM", 								true};
	{"mi-24p_ext06_BN",	ROUGHNESS_METALLIC,		"../SGPSz/mi-24p_ext06_PL_RoughMet", 		false};
	{"mi-24p_ext06_BN", DECAL,	            	"../SGPSz/mi-24p_bn1_PL", 					false};	

-- mi-24p_ext07 
	{"mi-24p_ext07", 0, 						"../SGPSz/mi-24p_ext07_PL", 				false};
	{"mi-24p_ext07", 1, 						"mi_24p_ext07_NRM",									true};	
	{"mi-24p_ext07", ROUGHNESS_METALLIC, 		"../SGPSz/mi-24p_ext07_PL_RoughMet", 		false};
		
-- mi-24p_ext07_net
	{"mi-24p_ext07_net", DECAL, 				"../SGPSz/mi-24p_ext07_PL", 				false};
	
-- mi-24p_tex01
	{"mi-24p_tex01_clear", 0, 					"../SGPSz/mi-24p_tex01_PL",					false};
	{"mi-24p_tex01_clear", 1, 					"mi-24p_tex01_NRM",									true};
	{"mi-24p_tex01_clear", ROUGHNESS_METALLIC,	"mi-24p_tex01_old2_RoughMet",						true};	 
	
-- mi-24p_tex03
	{"mi-24p_tex03", 0,							"../SGPSz/mi-24p_tex03_PL", 				false};
	{"mi-24p_tex03", 1,							"mi-24p_tex03_old_NRM", 							true};
	{"mi-24p_tex03", ROUGHNESS_METALLIC, 		"mi-24p_tex03_old_RoughMet", 						true};
	
	{"mi-24p_tex03_clear", 0,					"../SGPSz/mi-24p_tex03_PL", 				false};
	{"mi-24p_tex03_clear", 1,					"mi-24p_tex03_old_NRM",								true};
	{"mi-24p_tex03_clear", ROUGHNESS_METALLIC,	"mi-24p_tex03_old_RoughMet", 						true};

-- mi-24p_tex04
	{"mi-24p_tex04 ", DIFFUSE,					"../SGPSz/mi-24p_tex04_PL", 				false};
	{"mi-24p_tex04 ", ROUGHNESS_METALLIC,		"mi-24p_tex04_w_RoughMet", 							true};

-- mi-24_p_blade
	{"mi-24p_blade_no_shadow", DIFFUSE,			"../SGPSz/mi-24p_blade_PL", 				false};
	{"mi-24p_blade_shad", DIFFUSE,				"../SGPSz/mi-24p_blade_shad",				false};

-- mi-24p_glass
	{"mi-24p_tex05", 14,						"../SGPSz/mi-24p_tex05_PL_diff", 			false};

-- mi-24p_evu 
	{"mi_24p_evu", DIFFUSE,						"../SGPSz/mi-24p_evu_PL", 					false};
	{"mi_24p_evu", NORMAL_MAP,					"mi-24p_evu_nrm", 									true};
	{"mi_24p_evu", ROUGHNESS_METALLIC,			"mi-24p_evu_RoughMet", 								true};

-- BD3-57_diff	
	{"BD3-57", DIFFUSE,							"../SGPSz/BD3-57_PL_diff", 					false};
	{"BD3-57", NORMAL_MAP,						"bd3-57nm", 										true};
	{"BD3-57", ROUGHNESS_METALLIC,				"../SGPSz/bd3-57_PL_RoughMet", 				false};

-- BD3-57_diff	
	{"BD3-57", DIFFUSE,							"../SGPSz/BD3-57_PL_diff", 					false};
	{"BD3-57", NORMAL_MAP,						"bd3-57nm", 										true};
	{"BD3-57", ROUGHNESS_METALLIC,				"../SGPSz/bd3-57_PL_RoughMet", 				false};
	
-- Pilot
	{"01 - Default", DIFFUSE,    				"../SGPSz/pilot_MI24_SGPSz", 				false};
	{"01 - Default", 1,    						"../SGPSz/pilot_MI24_PL_NRM", 				false};
	{"01 - Default", ROUGHNESS_METALLIC,		"../SGPSz/pilot_MI24_PL_RoughMet", 			false};
	{"gunner_ammo",	DIFFUSE,					"../SGPSz/gunner_mi24_ammo_PL", 			false};
	{"gunner_base",	DIFFUSE,					"../SGPSz/gunner_mi24_base_SGPSz", 			false};

-- UB-32A (Mi-24):
	{"UB_32A_24_2s", DIFFUSE,					"../SGPSz/ub_32a_24_PL_diff", 				false};
	{"UB_32A_24_2s", NORMAL_MAP,				"ub_32a_24nm", 										true};
	{"UB_32A_24_2s", ROUGHNESS_METALLIC,		"../SGPSz/ub_32a_24_PL_RoughMet", 			false};
	{"UB_32A_24", DIFFUSE,						"../SGPSz/ub_32a_24_PL_diff", 				false};
	{"UB_32A_24", NORMAL_MAP,					"ub_32a_24nm", 										true};
	{"UB_32A_24", ROUGHNESS_METALLIC,			"../SGPSz/ub_32a_24_PL_RoughMet", 			false};
	
-- UB-32A (Mi-24):
	{"UB_32A_24_2s", DIFFUSE,					"../SGPSz/ub_32a_24_PL_diff", 				false};
	{"UB_32A_24_2s", ROUGHNESS_METALLIC,		"../SGPSz/ub_32a_24_PL_RoughMet", 			false};
	{"UB_32A_24", DIFFUSE,						"../SGPSz/ub_32a_24_PL_diff", 				false};
	{"UB_32A_24", ROUGHNESS_METALLIC,			"../SGPSz/ub_32a_24_PL_RoughMet",			false};
	
-- PTB-450 tank:
	{"PTB-450",	DIFFUSE,						"../SGPSz/PTB_450_PL_diff", 				false};
	{"PTB-450",	ROUGHNESS_METALLIC,				"../SGPSz/ptb_450_PL_RoughMet", 			false};

-- PTB-kA-50 tank:	
	{"PTB_kA-50", DIFFUSE,						"../SGPSz/PTB_KA-50_PL_diff", 				false};
	
-- 9M114 Shturm:
	{"9m114_pylon", DIFFUSE,					"../SGPSz/9M114-pylon_PL_diff", 			false};

-- 9M120 ATAKA:
	{"9m120_pylon", DIFFUSE,					"../SGPSz/9m120_Pylon_PL_diff", 			false};
	{"9m120_caps", DIFFUSE,						"../SGPSz/9m120_caps_PL_diff", 				false};
	
-- B-13l rocket pod:
	{"B-13L", DIFFUSE,							"../SGPSz/b-13l_PL_dif", 					false};
	{"B-13L", 2,								"../SGPSz/b-13l_PL_spec", 					false};
	
-- B8V20 rocket pod:
    {"b8v20", DIFFUSE,  	 					"../SGPSz/b8v20_PL_diff", 					false};
    {"b8v20", ROUGHNESS_METALLIC,   			"../SGPSz/b8v20_PL_RoughMet", 				false};

-- GUV-7800 Gun pod:
	{"GUV-7800_main", DIFFUSE,					"../SGPSz/guv-7800_PL", 					false};
	{"GUV-7800_main", 2,						"../SGPSz/guv-7800 spec", 					false};
}

name = "SGPSz - EVU Off"
-- countries = {"POL", "RED", "BLUE"}


custom_args = 
{
	[457] = 0.0, -- 0 = EVU off / 1 = EVU on
}