livery = {

-- AH-64D_bottom_1
	{"AH-64D_bottom_1",	DIFFUSE,			"ah-64d_PL_bottom_1", false};
	{"AH-64D_bottom_1",	NORMAL_MAP,			"ah-64d_bottom_1_normal", true};
	{"AH-64D_bottom_1",	ROUGHNESS_METALLIC,	"ah-64d_PL_bottom_1_roughmet", false};
	
-- AH-64D_bottom_2
	{"AH-64D_bottom_2",	DIFFUSE,			"ah-64d_PL_bottom_2", false};
	{"AH-64D_bottom_2",	NORMAL_MAP,			"ah-64d_bottom_2_normal", true};
	{"AH-64D_bottom_2",	ROUGHNESS_METALLIC,	"ah-64d_PL_bottom_2_roughmet", false};

--AH-64D_balka_1
	{"AH-64D_balka_1",	DIFFUSE,			"ah-64d_PL_balka_1", false};
	{"AH-64D_balka_1",	NORMAL_MAP,			"ah-64d_balka_1_normal", true};
	{"AH-64D_balka_1",	ROUGHNESS_METALLIC,	"ah-64d_PL_balka_1_roughmet", false};
	
--AH-64D_balka_2	
	{"AH-64D_balka_2",	DIFFUSE,			"ah-64d_PL_balka_2", false};
	{"AH-64D_balka_2",	NORMAL_MAP,			"ah-64d_balka_2_normal", true};
	{"AH-64D_balka_2",	ROUGHNESS_METALLIC,	"ah-64d_PL_balka_2_roughmet", false};

-- PILOT-------------------------------------------------------------------------------------------

-- Bailed
	{"pilot_AH64",	DIFFUSE,					"ah-64d_PL_pilot_body", 					false};
	{"pilot_AH64",	NORMAL_MAP,					"ah-64d_PL_pilot_body_normal", 				false};
	{"pilot_AH64",	SPECULAR,					"ah-64d_PL_pilot_body_roughmet", 			false};
	{"pilot_AH64",	ROUGHNESS_METALLIC,			"ah-64d_PL_pilot_body_roughmet", 			false};

	{"pilot_AH64_patch",	DIFFUSE,			"ah-64d_PL_pilot_patch_airborne", 			false};
	{"pilot_AH64_patch",	NORMAL_MAP,			"ah-64d_PL_pilot_patch_airborne_normal",	false};
	{"pilot_AH64_patch",	SPECULAR,			"ah-64d_PL_pilot_patch_airborne_roughmet",	false};
	{"pilot_AH64_patch",	ROUGHNESS_METALLIC,	"ah-64d_PL_pilot_patch_airborne_roughmet",	false};

	{"AH-64D_pilot_patch",	DIFFUSE,			"ah-64d_PL_pilot_patch_airborne", 			false};
	{"AH-64D_pilot_patch",	NORMAL_MAP,			"ah-64d_PL_pilot_patch_airborne_normal",	false};
	{"AH-64D_pilot_patch",	SPECULAR,			"ah-64d_PL_pilot_patch_airborne_roughmet",	false};
	{"AH-64D_pilot_patch",	ROUGHNESS_METALLIC,	"ah-64d_PL_pilot_patch_airborne_roughmet",	false};

	
-- TPP
	{"AH-64D_pilot_body",	DIFFUSE,					"ah-64d_PL_pilot_body", false};
	{"AH-64D_pilot_body",	NORMAL_MAP,					"ah-64d_PL_pilot_body_normal", false};
	{"AH-64D_pilot_body",	ROUGHNESS_METALLIC,			"ah-64d_PL_pilot_body_roughmet", false};

	{"AH-64D_pilot_details",	DIFFUSE,				"ah-64d_PL_pilot_details", false};
	{"AH-64D_pilot_details",	NORMAL_MAP,				"ah-64d_pilot_details_normal", true};
	{"AH-64D_pilot_details",	ROUGHNESS_METALLIC,		"ah-64d_pilot_details_roughmet", true};

	{"AH-64D_pilot_helmet_details",	DIFFUSE			,	"ah-64d_PL_pilot_helmet_details", false};
	{"AH-64D_pilot_helmet_details",	NORMAL_MAP			,	"ah-64d_pilot_helmet_details_normal", true};
	{"AH-64D_pilot_helmet_details",	SPECULAR			,	"ah-64d_pilot_helmet_details_roughmet", true};

	{"AH-64D_pilot_helmet_shell",	DIFFUSE				,	"ah-64d_PL_pilot_helmet_shell", false};
	{"AH-64D_pilot_helmet_shell",	NORMAL_MAP			,	"ah-64d_pilot_helmet_shell_normal", true};
	{"AH-64D_pilot_helmet_shell",	SPECULAR			,	"ah-64d_pilot_helmet_shell_roughmet", true};
	
	{"AH-64D_pilot_helmet_glass",	DIFFUSE,			"ah-64d_pilot_glass", true};
	{"AH-64D_pilot_helmet_glass",	ROUGHNESS_METALLIC,	"ah-64d_pilot_glass_roughmet", true};
	
--BORT_NUMBER----------------------------------------------------------------------------------------
	{"AH64D_decal_0",	DIFFUSE,			"AH-64D_decal_0", true};
	{"AH64D_fin_bort_number",	DIFFUSE,	"AH-64D_number", true};	
}

name = "Polish 1BLWL"
--countries = {"USA","POL"}

order     = 999

custom_args = 
{
 
[508] = 1.0, -- Pilot helmet visor
[518] = 1.0, -- Copilot helmet visor
[1000] = 0.0, -- change of type of board number (0.0 -default USA, 0.1- )
[1001] = 0.0, -- vis refuel board number 
[1002] = 1.0, -- change of type intake board number 
[1003] = 0.0, -- vis nouse board number 
}