livery = {
	{"BH_Base",			DIFFUSE,	"bh_base", false};

	{"BH_Fuselage1",	DIFFUSE,	"bh_fuselage1", false};

	{"BH_Fuselage2",	DIFFUSE,	"bh_fuselage2", false};

	{"BH_Fuselage3",	DIFFUSE,	"bh_fuselage3", false};

	{"BH_Fuselage4",	DIFFUSE,	"bh_fuselage4", false};

	{"BH_Fuselage5",	DIFFUSE,	"bh_fuselage5", false};

	{"BH_Fuselage6",	DIFFUSE,	"bh_fuselage6", false};

	{"BH_Fuselage7",	DIFFUSE,	"bh_fuselage7", false};

	{"BH_ESS",			DIFFUSE,	"bh_ess", false};
	{"BH_Probe",		DIFFUSE,	"bh_probe", false};
	{"BH_tank230",		DIFFUSE,	"bh_tank230", false};
	{"BH_Rotors",		DIFFUSE,	"bh_rotors", false};
	{"rotorblur",		DIFFUSE,	"h60_rotorblur", false};

	{"Mat_arm",	DIFFUSE			,	"tex_arm_pilot_multi", false};
	{"Mat_arm.003",	DIFFUSE			,	"tex_arm_copilot_multi", false};
	{"Mat_body",	DIFFUSE			,	"tex_body_multi", false};	
	{"Mat_helmet_nvg",	DIFFUSE			,	"tex_helmet_nvg_darkgrey", true};
	{"Mat_helmet.002",	DIFFUSE			,	"tex_helmet_darkgrey", true};
}
name = "US-ARMY-Afganistan 2 MC"

-- By Mitch
-- Moded by Mizuri