livery = {
    {"mi_8_tex1", DIFFUSE ,"mi_8_tex_1_PAM",false};
	{"glass_mi-8",	DIFFUSE	, "mi_8_glass_PAM", false};

	{"mi_8_tex2", DIFFUSE ,"mi_8_tex_2_PAM",false};
	{"mi_8_glass",	DIFFUSE	, "mi_8_glass_PAM", false};
	
	{"mi_8_tex3", DIFFUSE ,"mi_8_tex_3_PAM",false};
	{"mi-8_tex3", 0, "mi_8_tex_3_PAM", false};
	{"mi-8_tex_3", 0, "mi_8_tex_3_PAM", false};
	{"mi_8_glass",	DIFFUSE	, "mi_8_glass_PAM", false};

	{"mi_8_tex4", DIFFUSE ,"mi_8_tex_4_PAM",false};
	
	{"EVU", DIFFUSE , "evu_texture_PAM", false};
	
	{"ZSH-7",	DIFFUSE			,	"ZSH-7_Helmet_PAM", false};
	{"pilot_MI8_body",	DIFFUSE			,	"pilot_MI8_PAM", false};
	{"pilot_MI8_patch",	DIFFUSE			,	"pilot_MI8_patch_PAM", false};

	{"mi_8_tex1",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"rotors_opacity", DIFFUSE, "mi_8_rotors_opa_PAM", false};

	{"mi_8_BORT_NUMBER_100",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_BORT_NUMBER_001",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_BORT_NUMBER_010",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_ENGINE_NUMBER_100",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_ENGINE_NUMBER_001",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_ENGINE_NUMBER_010",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_SMALL_NUMBER_100",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_SMALL_NUMBER_001",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};
	{"mi_8_SMALL_NUMBER_010",	AMBIENT_OCCLUSION	,	"mi_8_tex_1_ao_PAM", false};

	
	-- Serial Numbers
	-- 3-digit tail number. location - in the area of engine air intakes
	{"mi_8_ENGINE_NUMBER_001", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_ENGINE_NUMBER_001", DECAL ,"Numbers_PAM",false};

    {"mi_8_ENGINE_NUMBER_010", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_ENGINE_NUMBER_010", DECAL ,"Numbers_PAM",false};

    {"mi_8_ENGINE_NUMBER_100", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_ENGINE_NUMBER_100", DECAL ,"Numbers_PAM",false};


	-- 3-digit tail number. location - on the board under the engine intake
	{"mi_8_SMALL_NUMBER_001", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_SMALL_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_SMALL_NUMBER_010", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_SMALL_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_SMALL_NUMBER_100", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_SMALL_NUMBER_100", DECAL ,"empty",true};


    -- 3-digit tail number. location - in the center of the body on board
	{"mi_8_BORT_NUMBER_001", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_BORT_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BORT_NUMBER_010", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_BORT_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BORT_NUMBER_100", 0 ,"mi_8_tex_1_PAM",false};
	{"mi_8_BORT_NUMBER_100", DECAL ,"empty",true};          


	-- 3-digit tail number. location - at the root of the tail bottom
	{"mi_8_BALKA_2_1_NUMBER_001", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_2_1_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_2_1_NUMBER_010", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_2_1_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_2_1_NUMBER_100", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_2_1_NUMBER_100", DECAL ,"empty",true};


	-- 3-digit tail number. location - on the tail bottom on the left side     
	{"mi_8_BALKA_1_1_NUMBER_001", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_1_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_1_1_NUMBER_010", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_1_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_1_1_NUMBER_100", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_1_NUMBER_100", DECAL ,"empty",true};

	
    -- 3-digit tail number. location - on the tail bottom to the right             	
	{"mi_8_BALKA_1_2_NUMBER_001", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_2_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_1_2_NUMBER_010", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_2_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_1_2_NUMBER_100", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_2_NUMBER_100", DECAL ,"empty",true};
	
	
	-- 3-digit tail number. location - the middle of the tail bottom to the right
	{"mi_8_BALKA_1_3_NUMBER_001", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_3_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_1_3_NUMBER_010", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_3_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_1_3_NUMBER_100", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_BALKA_1_3_NUMBER_100", DECAL ,"empty",true};

	-- 4-digit registration number. location - on the tail botom to the horizontal tail
	{"mi_8_SMALL_BALKA_NUMBER_001", 0 ,"mi_8_tex_2_PAM",false};            
	{"mi_8_SMALL_BALKA_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_SMALL_BALKA_NUMBER_010", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_SMALL_BALKA_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_SMALL_BALKA_NUMBER_100", 0 ,"mi_8_tex_2_PAM",false};
	{"mi_8_SMALL_BALKA_NUMBER_100", DECAL ,"empty",true};

}
name = "PAM2Air"

custom_args = {
	[26] = 0.0, --okno tylne drzwi
	[38] = 0.0,	-- drzwi boczne
	[80] = 0.0, -- opancerzenie
	[215] = 0.0, -- belka prawa
	[225] = 0.0, -- belka lewa
	[250] = 0.0, -- tylnie drzwi
	[457] = 0.0, -- EVU on
	[1000] = -1.0 -- belki obie
}

-- livery by Mizuri
