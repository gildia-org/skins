-- Livery by Raven
-- Pilot helmets and modified EVU ring by SundownerPL

livery = {
    {"mi_8_tex1", 0 ,"../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_tex1", NORMAL_MAP ,"../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_tex1", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	
	{"mi_8_tex2", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_tex2", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_tex2", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	
	{"mi_8_tex3", 0 ,"../SGPSz/mi_8_tex_3_PL",false};
	{"mi_8_tex3", NORMAL_MAP ,"../SGPSz/mi_8_tex_3_nm",false};
	{"mi_8_tex3", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_3_RoughMet",false};
	
	{"mi_8_tex4", 0 ,"../SGPSz/mi_8_tex_4_PL",false};
	{"mi_8_tex4", NORMAL_MAP ,"../SGPSz/mi_8_tex_4_nm",false};
	{"mi_8_tex4", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_4_RoughMet",false};

	{"EVU", 0 , "../SGPSz/evu_texture_PL", false};
	{"EVU", 2 ,"../SGPSz/evu_texture_spec",false};
	{"EVU", ROUGHNESS_METALLIC ,"../SGPSz/evu_texture_PL_RoughMet", false};

-- Pilot
	{"ZSH-7", 0 ,"../SGPSz/ZSH-7_PL",false};

	{"pilot_MI8_body", 0 ,"../SGPSz/pilot_mi8_PL",false};
	{"pilot_MI8_02",    DIFFUSE ,	"../SGPSz/pilot_mi8_02_PL", false};
	{"pilot_MI8_patch",	DIFFUSE	,	"../SGPSz/pilot_mi8_patch_PL", false};
	
-- Uzbrojenie
	-- GUV-7800 G/M
    {"GUV-7800_main",	0,   "../../Mi-24P/SGPSz/guv-7800_PL", false};
    {"GUV-7800_main",	2,   "../../Mi-24P/SGPSz/guv-7800 spec", false};

	-- UPK_23_250
    {"upk_23_250",		0,   "../SGPSz/UPK_23_250_PL", false};
    {"upk_23_250",		2,   "../SGPSz/UPK_23_250_spec_PL", false};

	-- B8V20:
    {"b8v20",    		0,   "../../Mi-24P/SGPSz/b8v20_PL_diff", false};
    {"b8v20",		 	ROUGHNESS_METALLIC ,   "../../Mi-24P/SGPSz/b8v20_PL_RoughMet", false};
	
-- Serial Numbers
	-- 3-digit tail number. location - in the area of engine air intakes
	{"mi_8_ENGINE_NUMBER_001", 0 ,"../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_ENGINE_NUMBER_001", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_ENGINE_NUMBER_001", NORMAL_MAP ,"../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_ENGINE_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_ENGINE_NUMBER_010", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_ENGINE_NUMBER_010", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_ENGINE_NUMBER_010", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_ENGINE_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_ENGINE_NUMBER_100", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_ENGINE_NUMBER_100", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_ENGINE_NUMBER_100", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_ENGINE_NUMBER_100", DECAL ,"empty",true};


	-- 3-digit tail number. location - on the board under the engine intake
	{"mi_8_SMALL_NUMBER_001", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_SMALL_NUMBER_001", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_SMALL_NUMBER_001", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_SMALL_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_SMALL_NUMBER_010", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_SMALL_NUMBER_010", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_SMALL_NUMBER_010", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_SMALL_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_SMALL_NUMBER_100", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_SMALL_NUMBER_100", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_SMALL_NUMBER_100", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_SMALL_NUMBER_100", DECAL ,"empty",true};


    -- 3-digit tail number. location - in the center of the body on board
	{"mi_8_BORT_NUMBER_001", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_BORT_NUMBER_001", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_BORT_NUMBER_001", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_BORT_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BORT_NUMBER_010", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_BORT_NUMBER_010", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_BORT_NUMBER_010", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_BORT_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BORT_NUMBER_100", 0 , "../SGPSz/mi_8_tex_1_SGPSZ",false};
	{"mi_8_BORT_NUMBER_100", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_1_SGPSz_RoughMet_halfgloss",false};
	{"mi_8_BORT_NUMBER_100", NORMAL_MAP , "../SGPSz/mi_8_tex_1_nm",false};
	{"mi_8_BORT_NUMBER_100", DECAL ,"empty",true};          


	-- 3-digit tail number. location - at the root of the tail bottom
	{"mi_8_BALKA_2_1_NUMBER_001", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_2_1_NUMBER_001", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_2_1_NUMBER_001", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_2_1_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_2_1_NUMBER_010", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_2_1_NUMBER_010", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_2_1_NUMBER_010", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_2_1_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_2_1_NUMBER_100", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_2_1_NUMBER_100", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_2_1_NUMBER_100", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_2_1_NUMBER_100", DECAL ,"empty",true};


	-- 3-digit tail number. location - on the tail bottom on the left side     
	{"mi_8_BALKA_1_1_NUMBER_001", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_1_NUMBER_001", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_1_NUMBER_001", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_1_NUMBER_001", DECAL ,"../SGPSz/number black reg",false};

    {"mi_8_BALKA_1_1_NUMBER_010", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_1_NUMBER_010", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_1_NUMBER_010", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_1_NUMBER_010", DECAL ,"../SGPSz/number black reg",false};

    {"mi_8_BALKA_1_1_NUMBER_100", 0 , "../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_1_NUMBER_100", ROUGHNESS_METALLIC , "../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_1_NUMBER_100", NORMAL_MAP , "../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_1_NUMBER_100", DECAL ,"../SGPSz/number black reg",false};

	
    -- 3-digit tail number. location - on the tail bottom to the right             	
	{"mi_8_BALKA_1_2_NUMBER_001", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_2_NUMBER_001", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_2_NUMBER_001", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_2_NUMBER_001", DECAL ,"../SGPSz/number black reg",false};

    {"mi_8_BALKA_1_2_NUMBER_010", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_2_NUMBER_010", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_2_NUMBER_010", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_2_NUMBER_010", DECAL ,"../SGPSz/number black reg",false};

    {"mi_8_BALKA_1_2_NUMBER_100", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_2_NUMBER_100", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_2_NUMBER_100", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_2_NUMBER_100", DECAL ,"../SGPSz/number black reg",false};
	
	
	-- 3-digit tail number. location - the middle of the tail bottom to the right
	{"mi_8_BALKA_1_3_NUMBER_001", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_3_NUMBER_001", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_3_NUMBER_001", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_3_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_BALKA_1_3_NUMBER_010", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_3_NUMBER_010", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_3_NUMBER_010", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_3_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_BALKA_1_3_NUMBER_100", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_BALKA_1_3_NUMBER_100", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_BALKA_1_3_NUMBER_100", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_BALKA_1_3_NUMBER_100", DECAL ,"empty",true};

	-- 4-digit registration number. location - on the tail botom to the horizontal tail
	{"mi_8_SMALL_BALKA_NUMBER_001", 0 ,"../SGPSz/mi_8_tex_2_PL",false};            
	{"mi_8_SMALL_BALKA_NUMBER_001", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_SMALL_BALKA_NUMBER_001", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_SMALL_BALKA_NUMBER_001", DECAL ,"empty",true};

    {"mi_8_SMALL_BALKA_NUMBER_010", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_SMALL_BALKA_NUMBER_010", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_SMALL_BALKA_NUMBER_010", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_SMALL_BALKA_NUMBER_010", DECAL ,"empty",true};

    {"mi_8_SMALL_BALKA_NUMBER_100", 0 ,"../SGPSz/mi_8_tex_2_PL",false};
	{"mi_8_SMALL_BALKA_NUMBER_100", ROUGHNESS_METALLIC ,"../SGPSz/mi_8_tex_2_RoughMet",false};
	{"mi_8_SMALL_BALKA_NUMBER_100", NORMAL_MAP ,"../SGPSz/mi_8_tex_2_nm",false};
	{"mi_8_SMALL_BALKA_NUMBER_100", DECAL ,"empty",true};
}
name = "SGPSz - Doors Off"

custom_args = {
	[38] = 1.0,		-- drzwi boczne
	[250] = 1.0, 	-- drzwi tylne
	[80] = 1.0,		-- opancerzenie
	-- [457] = 1.0, 	-- EVU
	
	-- [442] = 0.0,	-- numer x00
	-- [31] = 0.0,	-- numer 0x0
	-- [32] = 0.1,	-- numer 00x
}
