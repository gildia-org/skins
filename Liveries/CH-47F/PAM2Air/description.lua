livery = {

-- ch-47f_tex01
	{"ch-47f_tex01", 0, "ch-47f_tex01_PAM",	false};
	--{"ch-47f_tex01", 1, "ch-47f_tex01_NRM",	true};
	--{"ch-47f_tex01", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};	
	
-- ch-47f_tex01_BN
	{"ch-47f_tex01_BN", 0, "ch-47f_tex01_PAM",	false};
	--{"ch-47f_tex01_BN", 1, "ch-47f_tex01_NRM",	true};
	--{"ch-47f_tex01_BN", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};
	{"ch-47f_tex01_BN", 3 ,"ch-47f_Numbers_PAM", false};
	
-- ch-47f_tex01_DECAL
	{"ch-47f_tex01_DECAL", 0, "ch-47f_tex01_PAM",	false};
	--{"ch-47f_tex01_DECAL", 1, "ch-47f_tex01_NRM",	true};
	--{"ch-47f_tex01_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex01_RoughMet", true};	
	{"ch-47f_tex01_DECAL", DECAL, "ch-47f_decal_PAM",	false};
	
-- ch-47f_tex02
	{"ch-47f_tex02", 0, "ch-47f_tex02_PAM",	false};
	--{"ch-47f_tex02", 1, "ch-47f_tex02_NRM",	true};
	--{"ch-47f_tex02", ROUGHNESS_METALLIC, "ch-47f_tex02_RoughMet", true};	
	
-- ch-47f_tex03
	{"ch-47f_tex03", 0, "ch-47f_tex03_PAM",	false};
	--{"ch-47f_tex03", 1, "ch-47f_tex03_NRM",	true};
	--{"ch-47f_tex03", ROUGHNESS_METALLIC, "ch-47f_tex03_RoughMet", true};	
	
-- ch-47f_tex04
	{"ch-47f_tex04", 0, "ch-47f_tex04_PAM",	false};
	--{"ch-47f_tex04", 1, "ch-47f_tex04_NRM",	true};
	--{"ch-47f_tex04", ROUGHNESS_METALLIC, "ch-47f_tex04_RoughMet", true};		
	
-- ch-47f_tex05
	{"ch-47f_tex05", 0, "ch-47f_tex05_PAM",	false};
	--{"ch-47f_tex05", 1, "ch-47f_tex05_NRM",	true};
	--{"ch-47f_tex05", ROUGHNESS_METALLIC, "ch-47f_tex05_RoughMet", true};	
	
-- ch-47f_tex06
	{"ch-47f_tex06", 0, "ch-47f_tex06_PAM",	false};
	--{"ch-47f_tex06", 1, "ch-47f_tex06_NRM",	true};
	--{"ch-47f_tex06", ROUGHNESS_METALLIC, "ch-47f_tex06_RoughMet", true};		
	
-- ch-47f_tex07
	{"ch-47f_tex07", 0, "ch-47f_tex07_PAM",	false};
	--{"ch-47f_tex07", 1, "ch-47f_tex07_NRM",	true};
	--{"ch-47f_tex07", ROUGHNESS_METALLIC, "ch-47f_tex07_RoughMet", true};	

-- ch-47f_tex08
	{"ch-47f_tex08", 0, "ch-47f_tex08_PAM",	false};
	--{"ch-47f_tex08", 1, "ch-47f_tex08_NRM",	true};
	--{"ch-47f_tex08", ROUGHNESS_METALLIC, "ch-47f_tex08_RoughMet", true};	

-- ch-47f_tex09
	{"ch-47f_tex09", 0, "ch-47f_tex09_PAM",	false};
	--{"ch-47f_tex09", 1, "ch-47f_tex09_NRM",	true};
	--{"ch-47f_tex09", ROUGHNESS_METALLIC, "ch-47f_tex09_RoughMet", true};	
	
-- ch-47f_tex09_DECAL
	{"ch-47f_tex09_DECAL", 0, "ch-47f_tex09_PAM",	false};
	--{"ch-47f_tex09_DECAL", 1, "ch-47f_tex09_NRM",	true};
	--{"ch-47f_tex09_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex09_RoughMet", true};
	{"ch-47f_tex09_DECAL", DECAL, "ch-47f_decal_PAM",	false};
	
-- ch-47f_tex10
	{"ch-47f_tex10", 0, "ch-47f_tex10_PAM",	false};
	--{"ch-47f_tex10", 1, "ch-47f_tex10_NRM",	true};
	--{"ch-47f_tex10", ROUGHNESS_METALLIC, "ch-47f_tex10_RoughMet", true};	
	
-- ch-47f_tex11
	{"ch-47f_tex11", 0, "ch-47f_tex11_PAM",	false};
	--{"ch-47f_tex11", 1, "ch-47f_tex11_NRM",	true};
	--{"ch-47f_tex11", ROUGHNESS_METALLIC, "ch-47f_tex11_RoughMet", true};
	
-- ch-47f_tex11_BN
	{"ch-47f_tex11_BN", 0, "ch-47f_tex11_PAM",	false};
	--{"ch-47f_tex11_BN", 1, "ch-47f_tex11_NRM",	true};
	--{"ch-47f_tex11_BN", ROUGHNESS_METALLIC, "ch-47f_tex11_RoughMet", true};
	{"ch-47f_tex11_BN", 3 ,"ch-47f_Numbers_PAM_WT", false}; -- front top
	
-- ch-47f_tex12
	{"ch-47f_tex12", 0, "ch-47f_tex12_PAM",	false};
	--{"ch-47f_tex12", 1, "ch-47f_tex12_NRM",	true};
	--{"ch-47f_tex12", ROUGHNESS_METALLIC, "ch-47f_tex12_RoughMet", true};
	
-- ch-47f_tex13
	{"ch-47f_tex13", 0, "ch-47f_tex13_PAM",	false};
	--{"ch-47f_tex13", 1, "ch-47f_tex13_NRM",	true};
	--{"ch-47f_tex13", ROUGHNESS_METALLIC, "ch-47f_tex13_RoughMet", true};
	
-- ch-47f_tex14
	{"ch-47f_tex14", 0, "ch-47f_tex14_PAM",	false};
	--{"ch-47f_tex14", 1, "ch-47f_tex14_NRM",	true};
	--{"ch-47f_tex14", ROUGHNESS_METALLIC, "ch-47f_tex14_RoughMet", true};
	
-- ch-47f_tex15
	{"ch-47f_tex15", 0, "ch-47f_tex15_PAM",	false};
	--{"ch-47f_tex15", 1, "ch-47f_tex15_NRM",	true};
	--{"ch-47f_tex15", ROUGHNESS_METALLIC, "ch-47f_tex15_RoughMet", true};
	
-- ch-47f_tex16
	{"ch-47f_tex16", 0, "ch-47f_tex16_PAM",	false};
	--{"ch-47f_tex16", 1, "ch-47f_tex16_NRM",	true};
	--{"ch-47f_tex16", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	
-- ch-47f_tex16_BN - tylni rotor + tyl
	{"ch-47f_tex16_BN", 0, "ch-47f_tex16_PAM",	false};
	--{"ch-47f_tex16_BN", 1, "ch-47f_tex16_NRM",	true};
	--{"ch-47f_tex16_BN", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	{"ch-47f_tex16_BN", 3 ,"empty", true}; -- nose
	
-- ch-47f_tex16_DECAL
	{"ch-47f_tex16_DECAL", 0, "ch-47f_tex16_PAM",	false};
	--{"ch-47f_tex16_DECAL", 1, "ch-47f_tex16_NRM",	true};
	--{"ch-47f_tex16_DECAL", ROUGHNESS_METALLIC, "ch-47f_tex16_RoughMet", true};
	{"ch-47f_tex16_DECAL", DECAL, "ch-47f_decal_PAM",	false};	
	
-- ch-47f_tex17
	{"ch-47f_tex17", 0, "ch-47f_tex17_PAM",	false};
	--{"ch-47f_tex17", 1, "ch-47f_tex17_NRM",	true};
	--{"ch-47f_tex17", ROUGHNESS_METALLIC, "ch-47f_tex17_RoughMet", true};

-- ch-47f_tex19
	{"ch-47f_tex19_dmg",	DIFFUSE			,	"ch-47f_tex19_PAM", false};
	--{"ch-47f_tex19_dmg",	NORMAL_MAP			,	"ch-47f_tex19_nrm", false};
	--{"ch-47f_tex19_dmg",	DAMAGE				,	"ch-47f_dmg_blade_main", false};
	--{"ch-47f_tex19_dmg",	10,	"ch-47f_dmg_blade_nrm", false};
	
-- Pilot
	{"Pilot_CH47_Body",	DIFFUSE			,	"pilot_ch47_body_PAM", false};
	--{"Pilot_CH47_Body",	NORMAL_MAP			,	"pilot_ch47_body_normal", false};
	--{"Pilot_CH47_Body",	SPECULAR			,	"pilot_ch47_body_roughmet", false};

	{"Pilot_CH47_Details",	DIFFUSE			,	"pilot_ch47_details_PAM", false};
	--{"Pilot_CH47_Details",	NORMAL_MAP			,	"pilot_ch47_details_normal", false};
	--{"Pilot_CH47_Details",	SPECULAR			,	"pilot_ch47_details_roughmet", false};

-- Helmet
	{"Pilot_CH47_Helmet_Shell",	DIFFUSE			,	"pilot_ch47_helmet_shell_PAM", false};
	--{"Pilot_CH47_Helmet_Shell",	NORMAL_MAP			,	"pilot_ch47_helmet_shell_normal", false};
	--{"Pilot_CH47_Helmet_Shell",	SPECULAR			,	"pilot_ch47_helmet_shell_roughmet", false};


-- Patch
	{"Pilot_CH47_Patch",	DIFFUSE			,	"pilot_ch47_patch_PAM", false};
	{"Pilot_CH47_Patch",	NORMAL_MAP			,	"pilot_ch47_patch_PAM_normal", false};
	{"Pilot_CH47_Patch",	SPECULAR			,	"pilot_ch47_patch_PAM_roughmet", false};

}

name = "PAM2Air"

custom_args = {
	[26] = 0.0,	-- central hook door 0 - closed | 1 - open
	
	[38] = 0.0,	-- upper ramp door 0 - closed | 1 - open
	[86] = 0.0,	-- lower ramp 0 - folded | 0.65 - level | 1 - open
	[85] = 0.0,	-- ramps 0 - folded | 1 - unfolded
	
	[87] = 0.0,	-- left windows 0 - closed | 1 - open
	
	[348] = 0.0,	-- rear door 0 - closed | 1 - open
	[349] = 0.0,	-- side door top 0 - closed | 1 - open
	[350] = 0.0,	-- side door bottom 0 - closed | 1 - open
	
	[600] = 1.0,	-- benches 0 - unfolded | 1 - folded
}
-- livery by Mizuri