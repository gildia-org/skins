@echo off

ECHO Skrypt usuwajacy skiny z paczek Gildii 1.0.6, 1.0.7, 1.0.8. by Qba.

ECHO UWAGA - plik z tym skryptem musi byc umieszcozny w folderze \Saved Games\DCS.openbeta\Liveries


:choice
set /P c=Czy na pewno chcesz usunac wszystkie skiny z poprzednich i aktualnej gildiowej paczki? Bedziesz musial zainstalowac najnowsza paczke. [Y/N]?
if /I "%c%" EQU "Y" goto :delete_all_skins
if /I "%c%" EQU "N" goto :abort
goto :choice


:delete_all_skins


pause

::AH-64
echo Usuwanie skinow z folderu AH-64D_BLK_II
for %%s in (
"AH-64D_BLK_II\Polish 1BLWL\"
"AH-64D_BLK_II\3-ACS Troop A\"
"AH-64D_BLK_II\3-ACS Troop B\"
"AH-64D_BLK_II\3-ACS Troop C\" do if exist %%s rmdir %%s /q /s
echo Zakonczono

::AJS37
echo Usuwanie skinow z folderu AJS37
for %%s in (
"AJS37\SN-42 lucas\"
"AJS37\SN-42 Balthasar\"
"AJS37\SN-42 Robo\"
"AJS37\SN-42 ch0mikoo\"
"AJS37\SN-42 FastDust\" 
"AJS37\SN-42 TimyTall\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::AV8BNA
echo Usuwanie skinow z folderu AV8BNA
for %%s in (
"AV8BNA\VMA-273\"
"AV8BNA\73WH Base\"
"AV8BNA\73WH Ciupek\"
"AV8BNA\73WH Gru\"
"AV8BNA\73WH Ptako\"
"AV8BNA\73WH Talbot\"
"AV8BNA\73WH Wielbłąd\"
"AV8BNA\73WH Zorg\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::CH-47F
echo Usuwanie skinow z folderu CH-47F
for %%s in (
"CH-47F\PAM2Air\"
"CH-47F\Polish CH-47F 1BLWL\" ) do if exist %%s rmdir %%s /q /s
echo Zakonczono

::F-14A-135-GR
echo Usuwanie skinow z folderu F-14A-135-GR
for %%s in (
"F-14A-135-GR\VF31_101\"
"F-14A-135-GR\VF31_102\"
"F-14A-135-GR\VF31_103\"
"F-14A-135-GR\VF31_104\"
"F-14A-135-GR\VF31_105\"
"F-14A-135-GR\VF31_106\"
"F-14A-135-GR\VF31_107\"
"F-14A-135-GR\VF31_CAG\"
"F-14A-135-GR\VF31_CAG_100\"
"F-14A-135-GR\VF31_IRIAF\"
"F-14A-135-GR\VF31_NONUM\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::F-14b
echo Usuwanie skinow z folderu F-14b
for %%s in (
"F-14b\VF31_101\"
"F-14b\VF31_102\"
"F-14b\VF31_103\"
"F-14b\VF31_104\"
"F-14b\VF31_105\"
"F-14b\VF31_106\"
"F-14b\VF31_107\"
"F-14b\VF31_CAG\"
"F-14b\VF31_CAG_100\"
"F-14b\VF31_IRIAF\"
"F-14b\VF31_NONUM\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::F-15C
echo Usuwanie skinow z folderu F-15C
for %%s in (
"F-15C\F-15C 333rd Standard 2024\"
"F-15C\F-15C 333rd Standard\"
"F-15C\F-15 333rd Baseball H3\"
"F-15C\F-15 333rd Pawelek H3\"
"F-15C\F-15 333rd Standard H3\"
"F-15C\F-15 333rd Torres H3\"
"F-15C\F-15 333rd Vega H3\"
"F-15C\F-15 333rd VetisEC H3\"
"F-15C\F-15 333rd Vincent H3\"
"F-15C\F-15 333rd Woolf H3\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::F-15E
echo Usuwanie skinow z folderu F-15ESE
for %%s in (
"F-15ESE\F-15E 333rd Standard 2024\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::F-16C_50
echo Usuwanie skinow z folderu F-16C_50
for %%s in (
"F-16C_50\F-16C 333rd Standard 2024\"
"F-16C_50\F-16C 333rd Standard\"
"F-16C_50\44AG Aggressor\"
"F-16C_50\44AG Deafult\"
"F-16C_50\44AG IAF\"
"F-16C_50\333rd_Baseball\"
"F-16C_50\333rd_Paulek\"
"F-16C_50\333rd_Vega\"
"F-16C_50\333rd_Vincent\"
"F-16C_50\333rd_Woolf\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::FA-18C_hornet
echo Usuwanie skinow z folderu FA-18C_hornet
for %%s in (
"FA-18C_hornet\VFA-203 Aggressor 2\"
"FA-18C_hornet\VFA-203 CAG\"
"FA-18C_hornet\VFA-203 Line\"
"FA-18C_hornet\VFA-313 Hellhounds CAG\"
"FA-18C_hornet\VFA-313 Hellhounds\"
"FA-18C_hornet\VFA-418 Low Visibility2\"
"FA-18C_hornet\VFA-418 Low Visibility\"
"FA-18C_hornet\VFA-418 200\"
"FA-18C_hornet\VFA-418 Ags 202\"
"FA-18C_hornet\VFA-418 Foka 203\"
"FA-18C_hornet\VFA-418 kosa 214\"
"FA-18C_hornet\VFA-418 Low Visibility\"
"FA-18C_hornet\VFA-418 Rotein 206\"
"FA-18C_hornet\VFA-418 Skorpion 201\"
"FA-18C_hornet\VFA-418 Stivi 205\"
"FA-18C_hornet\VFA-418 Tofgre 211\"
"FA-18C_hornet\VMFA-117 Wombats\"
"FA-18C_hornet\VMFA-117 Wombats HiVis\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::ka-50
echo Usuwanie skinow z folderu ka-50
for %%s in (
"ka-50\3ES Emc\"
"ka-50\3ES FastDust\"
"ka-50\3ES Gildu\"
"ka-50\3ES Nervus\"
"ka-50\3ES Sadow\"
"ka-50\3ES Sancho\"
"ka-50\3ES Viking\") do if exist %%s rmdir %%s /q /s
echo Zakonczono


::mi-8mt
echo Usuwanie skinow z folderu mi-8mt
for %%s in (
"mi-8mt\PAM2Air\"
"mi-8mt\SGPSz - Doors On\"
"mi-8mt\SGPSz - Doors Off\"
"mi-8mt\SGPSz Guest\"
"mi-8mt\SGPSz\"
"mi-8mt\SGPSz_001\"
"mi-8mt\SGPSz_002\"
"mi-8mt\SGPSz_003\"
"mi-8mt\SGPSz_004\"
"mi-8mt\SGPSz_005\"
"mi-8mt\SGPSz_006\"
"mi-8mt\SGPSz_007\"
"mi-8mt\SGPSz_008\"
"mi-8mt\SGPSz_009\"
"mi-8mt\SGPSz_010\"
"mi-8mt\SGPSz_011\"
"mi-8mt\SGPSz_012\"
"mi-8mt\SGPSz_013\"
"mi-8mt\SGPSz_014\"
"mi-8mt\SGPSz_015\"
"mi-8mt\SGPSz_016\"
"mi-8mt\SGPSz_017\"
"mi-8mt\SGPSz_018\"
"mi-8mt\SGPSz_019\"
"mi-8mt\SGPSz_020\"
) do if exist %%s rmdir %%s /q /s
echo Zakonczono

::Mi-24P
echo Usuwanie skinow z folderu Mi-24P
for %%s in (
"Mi-24P\SGPSZ\"
"Mi-24P\SGPSz - EVU On\"
"Mi-24P\SGPSz - EVU Off\"
"Mi-24P\SGPSz Guest\"
"Mi-24P\SGPSZ - InBlack - PPKBlack\"
"Mi-24P\SGPSZ - InBlack - PPKBlue\"
"Mi-24P\SGPSZ - InBlack - PPKGreen\"
"Mi-24P\SGPSZ - InEyes - PPKBlack\"
"Mi-24P\SGPSZ - InEyes - PPKBlue\"
"Mi-24P\SGPSZ - InEyes - PPKGreen\"
"Mi-24P\SGPSZ - InGreen - PPKBlack\"
"Mi-24P\SGPSZ - InGreen - PPKBlue\"
"Mi-24P\SGPSZ - InGreen - PPKGreen\"
"Mi-24P\SGPSZ 1989-2006\"
"Mi-24P\SGPSZ 1989-2006 InBlack\"
"Mi-24P\SGPSZ 1989-2006 No eyes\"
"Mi-24P\SGPSZ EVU\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::MiG-21Bis
echo Usuwanie skinow z folderu MiG-21Bis
for %%s in (
"MiG-21Bis\9PLM\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::mig-29a
echo Usuwanie skinow z folderu mig-29a
for %%s in (
"mig-29a\827 IAP grey\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::mig-29s
echo Usuwanie skinow z folderu mig-29s
for %%s in (
"mig-29s\827 IAP - dark grey\"
"mig-29s\827 IAP - line\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::su-27
echo Usuwanie skinow z folderu su-27
for %%s in (
"su-27\827 IAP - K\"
"su-27\827 IAP - line\"
"su-27\827 IAP - old\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::uh-1h
echo Usuwanie skinow z folderu uh-1h
for %%s in (
"uh-1h\PAM2Air\"
"uh-1h\CSAR 61 Night Stalkers\"
"uh-1h\CSAR 62 Night Stalkers\"
"uh-1h\CSAR 63 Night Stalkers\"
"uh-1h\CSAR 64 Night Stalkers\"
"uh-1h\CSAR 65 Night Stalkers\"
"uh-1h\CSAR 66 Night Stalkers\"
"uh-1h\CSAR Poland Army Medevac\"
"uh-1h\CSAR Poland Army Modern\"
"uh-1h\CSAR Poland Army SAR\"
"uh-1h\CSAR Poland NAVY SAR\"
"uh-1h\CSAR Poland_2_GPR_0519_2013_W-3W_SAR\"
"uh-1h\CSAR USAF 66th RQS\"
"uh-1h\CSAR USAF 66th RQS 26463\"
"uh-1h\CSAR USAF 66th RQS_1\") do if exist %%s rmdir %%s /q /s
echo Zakonczono

::UH-60L
echo Usuwanie skinow z folderu UH-60L
for %%s in (
"UH-60L\US-ARMY-Afganistan 2 MC\") do if exist %%s rmdir %%s /q /s
echo Zakonczono



echo Skiny z poprzednich paczek 1.0.6, 1.0.7, 1.0.8 zostaly usuniete. Zainstaluj teraz najnowsza paczke.


:abort

echo Skrypt zakonczyl dzialanie.
pause










