# Malowania Gildia.org

## Instalacja
### Pobieranie
* Wchodzimy na https://gitlab.com/gildia-org/skins/-/releases
* Pobieramy "Source code (zip)" ostatniego wydania lub wersję "skompresowaną do maksimum"

### Dodawanie do DCS World 
Folder "Liveries" umieścić w "Saved Games\DCS.openbeta".

Standardowa lokalizacja:
* Wersja Standalone i steam nie różnią się lokalizacją
  * "%userprofile%\Saved Games\DCS.openbeta"

### Kompatybilność z poprzednimi wersjami oraz innymi malowaniami
**Zalecane jest usunięcie poprzednich wersji paczki malowań z SG lub gry.**
Dodatkowo zalecane jest usunięcie pojedyńczych paczek eskadrowych.
Nie gwarantujemy poprawności działania paczki bez wykonania tych czynności.


