### Lista zmian:


#### 1.0.8
* Dodano malowania PAM2Air
* Dodano malowania 1 BLWL
* Dodano malowania UH-60 od 1stAC
* Zmodyfikowano malowania VFS-333
* Usunięto malowania 3-ACS
* Usunięto malowania VFA-203
* Usunięto malowania 44th AG
* Usunięto malowania SN-42
* Usunięto część malowań UH-1 od 1stAC
* Usunięto część malowań VF-31



#### 1.0.7
* Dodano malowania VMA-273 Black Cubs
* Dodano malowania 3-ACS
* Zaktualizowano malowania SGPSz
* Zaktualizowania malowania VFS-333
* Zaktualizowania malowania VFA-418
* Zaktualizowania malowania 9.WPLM
* Zaktualizowano malowania 44th AG
* Usunięto malowania 827BTA
* Usunięto malowania 73rd WH
* Usunięto część malowań 1st AC
* Usunięto malowania ES3
* Usunięto listę polecanych dodatkowych malowań.

#### 1.0.6
* Zmienione dotychczasowee malowania Mi-24P oraz dodano kilka wariantów.
* Zaktualizowano malowania VFA-203.
* Zaktualizowano malowania VFA-313.
* Zaktualizowano malowania VMFA-117.
* Zaktualizowano malowania VFA-418.
* Zaktualizowano malowania VF-31.
* Malowania F-15C 333rd zostały zrobione od nowa.
* Dodano 6 malowań eskadry 827IAP (MiG-29A/S, Su-27)
* Usunięto 2 malowania SN-42 (Bartek16194, ElSanchoPancho)
* Usunięto 2 malowania VFA-418 (SirTea i Grooz)
* Usunięto malowania VBW-304.

#### 1.0.5
* Dodano listę polecanych malowań.
* Drobne poprawki pojedyńczych malowań.
* Dodano 6 malowań UH-1H eskadry CSAR.
* Dodano 1 malowanie F-18C eskadry VFA-313.
* Dodano 1 malowanie Mi-24P eskadry SGPSz.
* Rework malowań Mi-8 SGPSz.
* Dodano 7 malowań eskadry 73WH.
* Dodano 8 malowań eskadry VFA-418.

#### 1.0.4
* Usunięto malowania eskadry EC 1/15 ORLY.
* Dodano 8 malowań eskadry VF-31 dla F-14A/B. 4 low vis oraz 4 hi viz
* Dodano 8 malowań eskadry SN-42.


#### 1.0.3
* Przeniesiono malowania z folderu instalacyjnego gry do saved games.
* Usunięto malowania uzytkownika Paulek z eskadry VBW-304.
* Usunięto malowania eskadry VF-112
* Dodano dwa malowania VMFA-117
* Dodano jedno malowanie Ka-50 eskadry 3-ES
* Dodane jedno malowanie F-15C eskadry VFS-333
* Dodane jedno malowanie Mi-8 eskadry SPGSZ oraz kilka zmian w obecnych malowaniach.

#### 1.0.2
* Dodane pięć malowań F-16C eskadry VFS-333
* Poprawione pliki description.lua dla malowań VFA-418.

#### 1.0A
* Poprawione pliki description.lua dla malowań 44AG.
* Dodane roughmety naszywek do malowania VFA-418 Line.
* Poprawione malowania 3ES